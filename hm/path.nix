{
  home.sessionPath =
    [
      "$HOME/.config/emacs/bin"
      "$HOME/.local/bin"
      "$HOME/.local/bin/scripts"
      "$HOME/go/bin"
      "$HOME/.cargo/bin"
      "$HOME/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/bin"
    ];

}
