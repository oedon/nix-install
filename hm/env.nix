{
  home.sessionVariables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
    BROWSER = "chromium";
    MANPAGER = "nvim +Man!";
    MANWIDTH = "999";
    LESSHISTFILE = "-";
    # HISTFILE = "\${HOME}/.cache/zsh/history";
    ZDOTDIR = "\${HOME}/.config/zsh";
    ZSH = "\${HOME}/.config/zsh";
    ZSH_COMPDUMP = "\${HOME}/.local/share/zsh/.zcompdump";
    STARSHIP_CONFIG = "\${HOME}/.config/zsh/starship.toml";

    # Misc
    _JAVA_AWT_WM_NONREPARENTING = "1";
    CUDA_CACHE_PATH = "\${HOME}/.cache/nv";
    PASSWORD_STORE_DIR = "\${HOME}/.local/share/password-store";

    # XDG
    XDG_CONFIG_HOME = "\${HOME}/.config";
    XDG_DATA_HOME = "\${HOME}/.local/share";
    XDG_BIN_HOME = "\${HOME}/.local/bin";
    XDG_CACHE_HOME = "\${HOME}/.cache";

    # GO
    GOPATH = "\${HOME}/go";
    GOBIN = "\${HOME}/go/bin";

    # Rust
    CARGO_HOME = "\${HOME}/.cargo";
    RUSTUP_HOME = "\${HOME}/.rustup";

  };

}
