{ pkgs, config, ... }:
{
  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
    enableCompletion = true;
    enableSyntaxHighlighting = true;
    autocd = true;
    history.path = "${config.xdg.dataHome}/zsh/zsh_history";

    completionInit = "
        autoload -U compinit
        zstyle ':completion:*' menu select
        zmodload zsh/complist
        compinit
        _comp_options+=(globdots)
        ";

    plugins = [
      {
        name = "fzf-tab";
        src = pkgs.fetchFromGitHub {
          owner = "Aloxaf";
          repo = "fzf-tab";
          rev = "master";
          sha256 = "sha256-dPe5CLCAuuuLGRdRCt/nNruxMrP9f/oddRxERkgm1FE=";
        };
      }

    ];

    shellAliases = {
      cd = "z";
      cp = "cp -iv";
      mv = "mv -iv";
      rm = "rm -iv";
      ls = "lsd -al --color always --group-dirs first";
      la = "lsd -a --color always --group-dirs first";
      ll = "lsd -l --color always --group-dirs first";
      lt = "lsd -aT --color always --group-dirs first";
    };
  };
  
  programs.fzf = {
    enable = true;
    enableZshIntegration = true;
  };

  programs.lsd = {
    enable = true;
  };

  programs.zoxide = {
    enable = true;
    enableZshIntegration = true;
  };

  programs.starship = {
    enable = true;
    enableZshIntegration = true;
  };
}
