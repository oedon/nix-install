{ config, pkgs, ... }:
# Option to pull unstable packages from the unstable channel with a prefix
let
  unstable = import
    (builtins.fetchTarball https://github.com/nixos/nixpkgs/tarball/nixos-unstable)
    # reuse the current configuration
    { config = config.nixpkgs.config; };
in
{

  imports =
    [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ];
  nix.settings.experimental-features = [ "nix-command" "flakes"];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

# Enable the X11 windowing system. Set nvidia and OpenGL
services.xserver = {
  enable = true;
  videoDrivers = [ "nvidia" ];
  # Configure keymap in X11
  layout = "us";
  autoRepeatDelay = 300;
  autoRepeatInterval = 50;
  # xkbOptions = "grp:shifts_toggle, grp_led:caps, caps:escape";
  xkbOptions = "caps:escape";

  # Display & Window Manager
  displayManager.gdm.enable = true;
  desktopManager.gnome.enable = true;
  displayManager.defaultSession = "gnome";
  # windowManager.dwm.enable = true;
};
hardware.opengl.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  networking.hostName = "KO-Kraft"; # Define your hostname.
  networking.nameservers = [ "1.1.1.1" "9.9.9.9" ]; # Nameservers
  networking.networkmanager.enable = true;
  networking.firewall.extraCommands = ''iptables -t raw -A OUTPUT -p udp -m udp --dport 137 -j CT --helper netbios-ns'';
  networking.wireguard.enable = true;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp34s0.useDHCP = true;

# Select internationalisation properties.
i18n.defaultLocale = "en_US.UTF-8";
i18n.extraLocaleSettings = {
    LC_MESSAGES = "en_US.UTF-8";
    LC_MONETARY = "de_DE.UTF-8";
    LC_PAPER = "de_DE.UTF-8";
    LC_MEASUREMENT = "de_DE.UTF-8";
    LC_TIME = "de_DE.UTF-8";
    LC_NUMERIC = "de_DE.UTF-8";
};
console = {
  font = "Lat2-Terminus16";
  keyMap = "us";
};

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

# HEADER DO NOT CHANGE
programs = {

# GPG Agent
gnupg.agent.enable = true;

neovim.vimAlias = true;
neovim.viAlias = true;

# Start ssh agent at login for keepassxc
ssh.startAgent = true;

# virt-manager
dconf.enable = true;

# Zsh
zsh.enable = true;

# https://mobil.steinacker.com/EWS/Exchange.asmx
evolution.enable = true;
evolution.plugins = [ pkgs.evolution-ews ];

# FOOTER DO NOT CHANGE
};

# Services
  services = {

# Enable cron service
cron = {
  enable = true;
  systemCronJobs = [
   # "*/30 * * * *      kraftc    rclone copy /home/kraftc/Documents/Work linode:work/Documents --log-file=/home/kraftc/.config/rclone/work.txt"
   # "*/15 * * * *      kraftc    rclone copy /home/kraftc/Documents/Emacs linode:emacs-work --log-file=/home/kraftc/.config/rclone/emacs.txt"
  ];
 };

# Samba Service
gvfs.enable = true;
gnome.gnome-keyring.enable = true;

mullvad-vpn.enable = true;

# Enable CUPS to print documents. http://localhost:631/ ipp://192.168.1.80/ipp/print lpd://192.168.1.51/print C360 Konica
printing.enable = true;
# printing.drivers = [ pkgs.samsung-unified-linux-driver_1_00_37 ];

# Start emacs daemon
emacs.enable = true;
emacs.package = pkgs.emacs;

# Locate Service
locate = {
      enable = true;
      locate = pkgs.mlocate;
      interval = "hourly";
      localuser = null;
  };

# TRIM
fstrim = {
      enable = true;
      interval = "weekly";
  };

tailscale.enable = true;

udev.packages = with pkgs; [ gnome3.gnome-settings-daemon ];

flatpak.enable = true;

# FOOTER DO NOT CHANGE
};

# Libvirt for virt-manager
virtualisation.libvirtd.enable = true;
virtualisation.docker.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  security.sudo.wheelNeedsPassword = false; # NoPASSWD for Wheel
  users.users.kraftc = {
    isNormalUser = true;
    extraGroups = [ "wheel" "libvirtd" "mlocate" "networkmanager" "docker" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.zsh;
  };

# Pkgs Config
nixpkgs.config = {
  allowUnfree = true;
  joypixels.acceptLicense = true;
};

environment.systemPackages = with pkgs; [
  alacritty
  appimage-run
  bat #modern cat
  bitwarden
  brave
  cifs-utils #samba
  clang
  clang-tools
  cmake
  coreutils
  dconf #Gui gnome config
  delta #modern diff
  dig #Dig http query
  duf #df replacement
  fd
  file
  firefox
  fzf
  gcc
  gimp-with-plugins
  git
  glib
  glibc
  glslang
  gnome.dconf-editor
  gnupg
  gparted
  graphviz #org-roam
  gthumb
  htop
  joypixels
  killall
  kitty
  libclang
  # libsForQt5.kdenlive # KdenLive
  # llvmPackages_14.llvm
  lm_sensors
  lsd
  mlocate
  mpv
  mullvad
  mullvad-vpn
  mupdf
  neofetch
  nix-prefetch-scripts
  nixfmt #Emacs nix
  nox #better nix search
  nvd #Nix diff tool
  onlyoffice-bin
  openssh
  openssl
  p7zip
  pandoc
  pcmanfm
  perl534Packages.EmailOutlookMessage
  picom
  poppler
  rclone
  remmina
  ripgrep
  rustdesk
  rnix-lsp #Vim Nix LSP
  rofi
  samba
  scrot
  shellcheck
  sqlite
  sqlitebrowser
  tailscale
  tesseract
  texlive.combined.scheme-full
  ungoogled-chromium
  unstable.flameshot
  unstable.koreader
  unstable.librewolf
  unstable.logseq
  unstable.mullvad-browser
  unstable.neovim
  unstable.obsidian
  unstable.tor-browser-bundle-bin
  unstable.youtube-dl
  # unstable.starship
  unzip
  vim
  virt-manager
  wget
  wireguard-tools
  xclip
  xorg.setxkbmap
  xorg.xrandr
  xsel
  yt-dlp
  zlib
  zoxide
  zsh

  # GNOME
  gnome3.adwaita-icon-theme
  gnomeExtensions.appindicator
  gnomeExtensions.no-titlebar-when-maximized
  gnome.gnome-tweaks
  gnomeExtensions.user-themes
  gnomeExtensions.no-overview
  gnomeExtensions.mullvad-indicator
  gnomeExtensions.dash-to-dock-for-cosmic
  gnomeExtensions.openweather
  gnomeExtensions.bitcoin-markets
  gnome-online-accounts
  gruvbox-dark-gtk
  gruvbox-dark-icons-gtk
  papirus-icon-theme
  # arc-theme

  # Code
  tree-sitter

  # VS Code
  unstable.vscode
  unstable.vscode-extensions.ms-vscode.cpptools
  unstable.github-desktop

  # Lua
  lua
  sumneko-lua-language-server
  stylua

  # Npm
  unstable.nodePackages.npm
  unstable.nodePackages.prettier
  unstable.nodePackages.serve
  unstable.nodejs-18_x
  html-tidy
  nodePackages.stylelint
  nodePackages.js-beautify

  # Python
  python-language-server
  python3Full
  python310
  python310Packages.python-lsp-server
  python310Packages.pip
  python310Packages.flake8
  python310Packages.grip
  python310Packages.nose
  python310Packages.pytest
  python310Packages.isort
  black # Style formatter
  pipenv
  nodePackages.pyright

  # GO
  unstable.go
  unstable.gopls
  unstable.golangci-lint
  gocode
  gomodifytags
  gotests
  gore
  gotools

  # Javascript
  # unstable.jetbrains.webstorm
  unstable.nodePackages.eslint
  unstable.nodePackages.vscode-langservers-extracted
  unstable.nodePackages.typescript
  unstable.nodePackages.typescript-language-server
  unstable.nodePackages.live-server # Start live-server .
  unstable.yarn

  # Rust
  unstable.rustup # Setup: rustup install stable # rustup default stable
  unstable.rust-analyzer
];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?

}
