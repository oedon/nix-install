{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "kraftc";
  home.homeDirectory = "/home/kraftc";
  home.stateVersion = "22.11"; # Please read the comment before changing.

  imports = [
    ./env.nix
    ./git.nix
    ./nvim.nix
    ./packages.nix
    ./path.nix
    ./shell.nix
    ./term.nix
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file."${config.xdg.configHome}" = {
    source = ./dotfiles;
    recursive = true;
  };
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
