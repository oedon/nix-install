{ pkgs, ... }: {
  home.packages = with pkgs;  [
    neofetch
    duf
    (nerdfonts.override { fonts = [ "JetBrainsMono" ]; })
  ];
}
