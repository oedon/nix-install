{
  programs.git = {
    enable = true;
    userName = "Frank Drebin";
    userEmail = "gitlab@oedon.me";
    ignores = [ "*~" ".DS_Store" ".direnv" ".env" ];
    extraConfig = {
      init = { defaultBranch = "main"; };
    };
    delta = { enable = true; };
  };
}
